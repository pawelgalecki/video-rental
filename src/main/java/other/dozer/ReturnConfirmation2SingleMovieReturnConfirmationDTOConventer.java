package other.dozer;

import application.dto.MovieDTO;
import application.dto.MovieTypeDTO;
import application.dto.SingleMovieReturnConfirmationDTO;
import model.rent.ReturnConfirmation;
import org.dozer.DozerConverter;

public class ReturnConfirmation2SingleMovieReturnConfirmationDTOConventer extends DozerConverter<ReturnConfirmation,SingleMovieReturnConfirmationDTO>{

    public  ReturnConfirmation2SingleMovieReturnConfirmationDTOConventer() {
        super(ReturnConfirmation.class, SingleMovieReturnConfirmationDTO.class);

    }

    @Override
    public SingleMovieReturnConfirmationDTO convertTo(ReturnConfirmation source, SingleMovieReturnConfirmationDTO destination) {
        SingleMovieReturnConfirmationDTO dto = new SingleMovieReturnConfirmationDTO();
        MovieDTO movieDTO = new MovieDTO();
        movieDTO.setId(source.getMovie().getId().toString());
        movieDTO.setTitle(source.getMovie().getTitle());
        movieDTO.setType(MovieTypeDTO.valueOf(source.getMovie().getType().name()));
        dto.setExtraCharge(source.getExtraCharge());
        int extraDays = source.getActualNumberOfDays() - source.getPlannedNumberOfDays();
        dto.setExtraDays(extraDays<0?0:extraDays);
        dto.setMovie(movieDTO);
        return dto;
    }

    @Override
    public ReturnConfirmation convertFrom(SingleMovieReturnConfirmationDTO source, ReturnConfirmation destination) {
        return null;
    }
}
