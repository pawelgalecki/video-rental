package other.dozer;

import application.dto.CustomerDTO;
import model.Customer;
import org.dozer.CustomConverter;
import org.dozer.DozerConverter;

public class CustomerDTO2CustomerConverter extends DozerConverter<CustomerDTO, Customer> {


    public CustomerDTO2CustomerConverter() {
        super(CustomerDTO.class, Customer.class);
    }

    @Override
    public Customer convertTo(CustomerDTO source, Customer destination) {
        return new Customer(source.getFirstName(), source.getLastName(), source.getId());
    }

    @Override
    public CustomerDTO convertFrom(Customer source, CustomerDTO destination) {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(source.getFirstName());
        customerDTO.setLastName(source.getLastName());
        customerDTO.setId(source.getId());
        return customerDTO;
    }
}
