package other.dozer;

import application.RentalConfirmation;
import application.dto.*;
import model.Customer;
import model.rent.RentConfirmation;
import org.dozer.DozerConverter;

import java.util.ArrayList;
import java.util.function.Consumer;

public class RentalConfirmation2RentalConfirmationDTOConverter extends DozerConverter<RentalConfirmation, RentalConfirmationDTO> {


    public RentalConfirmation2RentalConfirmationDTOConverter() {
        super(RentalConfirmation.class, RentalConfirmationDTO.class);
    }


    @Override
    public RentalConfirmationDTO convertTo(RentalConfirmation source, RentalConfirmationDTO destination) {
        RentalConfirmationDTO rentalConfirmationDTO = new RentalConfirmationDTO();

        Customer customer = source.getRentedMovies().get(0).getCustomer();
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName(customer.getFirstName());
        customerDTO.setLastName(customer.getLastName());
        customerDTO.setId(customer.getId());
        rentalConfirmationDTO.setCustomer(customerDTO);

        ArrayList<RentedMovieDTO> rentedMovieDTOs = new ArrayList<>();
        source.getRentedMovies().forEach(rentConfirmation -> {
            RentedMovieDTO rentedMovieDTO = new RentedMovieDTO();
            MovieDTO movieDTO = new MovieDTO();
            rentedMovieDTO.setInitialCharge(rentConfirmation.getInitialCharge());
            rentedMovieDTO.setPlannedNumberOfDays(rentConfirmation.getPlannedNumberOfDays());
            movieDTO.setId(rentConfirmation.getMovie().getId().toString());
            movieDTO.setTitle(rentConfirmation.getMovie().getTitle());
            movieDTO.setType(MovieTypeDTO.valueOf(rentConfirmation.getMovie().getType().name()));
            rentedMovieDTO.setMovie(movieDTO);
            rentedMovieDTOs.add(rentedMovieDTO);
        });

        rentalConfirmationDTO.setRentedMovies(rentedMovieDTOs);
        rentalConfirmationDTO.setTotal(source.getTotal());
        return rentalConfirmationDTO;

    }

    @Override
    public RentalConfirmation convertFrom(RentalConfirmationDTO source, RentalConfirmation destination) {
        return null;
    }
}
