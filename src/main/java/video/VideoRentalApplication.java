package video;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication (scanBasePackages= {"adapters.rest","application","services", "repositories"})
@ImportResource("classpath:dozerConfig.xml")
public class VideoRentalApplication {
	public static void main(String[] args) {
		SpringApplication.run(VideoRentalApplication.class, args);
	}

}
