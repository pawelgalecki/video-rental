package repositories;

import model.Movie;
import model.MovieType;
import org.springframework.stereotype.Repository;
import services.exceptions.MovieAlreadyReturnedException;
import services.exceptions.MovieNotAvailableException;
import services.exceptions.MovieWasNotRentedHereException;
import services.exceptions.TitleUnknownException;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.stream.Collectors;

@Repository
public class Inventory {
    private Map<Movie, Boolean> inventory = new HashMap<>();



    public void addMovie(String title, MovieType type, UUID uuid) {
        inventory.put(new Movie(title, type,uuid), true);
    }


    public Movie findAvailableCopyOf(String title) {

        Map<Movie, Boolean> matchingTitles = inventory.entrySet().stream().filter(movieBooleanEntry -> movieBooleanEntry.getKey().getTitle().equals(title))
                .collect(Collectors.toMap(Entry::getKey, Entry::getValue));

        if (matchingTitles.size() == 0) {
            throw new TitleUnknownException(title);
        } else {

            Movie movie = matchingTitles.entrySet().stream().filter(z -> z.getValue().equals(true)).map(Entry::getKey).findFirst().orElseThrow(()->new MovieNotAvailableException(title));
            inventory.put(movie, false);
            return movie;
        }

    }

    public void giveBack(Movie movie) {
        Entry<Movie, Boolean> foundEntry = inventory.entrySet().stream().filter(entry -> entry.getKey().equals(movie)).findFirst().orElseThrow(() -> new MovieWasNotRentedHereException(movie));
        if (foundEntry.getValue()) {
            throw new MovieAlreadyReturnedException(movie);
        } else {
            foundEntry.setValue(true);
        }
    }


    public void checkThatMovieCanBeGivenBack(Movie movie) {
        Entry<Movie, Boolean> foundEntry = inventory.entrySet().stream().filter(entry -> entry.getKey().equals(movie)).findFirst().orElseThrow(() -> new MovieWasNotRentedHereException(movie));
        if (foundEntry.getValue()) {
            throw new MovieAlreadyReturnedException(movie);
        }
    }
}
