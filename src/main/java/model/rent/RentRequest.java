package model.rent;

import lombok.Data;
import lombok.Value;
import model.Customer;
import model.Movie;

@Value
public class RentRequest {
    private final Customer customer;
    private final Movie movie;
    private final Integer plannedNumberOfDays;
}
