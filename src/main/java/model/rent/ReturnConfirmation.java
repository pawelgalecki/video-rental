package model.rent;

import lombok.Value;
import model.Customer;
import model.Movie;

import java.math.BigDecimal;

@Value
public class ReturnConfirmation {

    private final BigDecimal extraCharge;
    private final Integer actualNumberOfDays;
    private final BigDecimal initialCharge;
    private final Customer customer;
    private final Movie movie;
    private final Integer plannedNumberOfDays;


    public ReturnConfirmation(BigDecimal extraCharge, Integer actualNumberOfDays, BigDecimal initialCharge, Customer customer, Movie movie, Integer plannedNumberOfDays) {
        this.extraCharge = extraCharge;
        this.actualNumberOfDays = actualNumberOfDays;
        this.initialCharge = initialCharge;
        this.customer = customer;
        this.movie = movie;
        this.plannedNumberOfDays = plannedNumberOfDays;
    }

    public static ReturnConfirmationBuilder from(ReturnRequest returnRequest) {
        return new ReturnConfirmationBuilder(returnRequest);
    }

    public static class ReturnConfirmationBuilder {

        private ReturnRequest returnRequest;


        public ReturnConfirmationBuilder(ReturnRequest returnRequest) {
            this.returnRequest = returnRequest;
        }

        public ReturnConfirmation withExtraCharge(BigDecimal extraCharge) {
            return new ReturnConfirmation(extraCharge, returnRequest.getActualNumberOfDays(), returnRequest.getInitialCharge(), returnRequest.getCustomer(), returnRequest.getMovie(), returnRequest.getPlannedNumberOfDays());
        }

    }


}
