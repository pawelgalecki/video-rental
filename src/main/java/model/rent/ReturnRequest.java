package model.rent;

import lombok.Value;
import model.Customer;
import model.Movie;

import java.math.BigDecimal;

@Value
public class ReturnRequest {

    private BigDecimal initialCharge;
    private final Customer customer;
    private final Movie movie;
    private final Integer plannedNumberOfDays;
    private final Integer actualNumberOfDays;

    @java.beans.ConstructorProperties({"initialCharge", "customer", "movie", "plannedNumberOfDays", "actualNumberOfDays"})
    private ReturnRequest(BigDecimal initialCharge, Customer customer, Movie movie, Integer plannedNumberOfDays, Integer actualNumberOfDays) {
        this.initialCharge = initialCharge;
        this.customer = customer;
        this.movie = movie;
        this.plannedNumberOfDays = plannedNumberOfDays;
        this.actualNumberOfDays = actualNumberOfDays;
    }

    public static ReturnRequestBuilder from(RentConfirmation rentConfirmation) {
        return new ReturnRequestBuilder(rentConfirmation);
    }

    public static class ReturnRequestBuilder {

        private RentConfirmation returnConfirmation;

        ReturnRequestBuilder(RentConfirmation returnConfirmation) {
            this.returnConfirmation = returnConfirmation;
        }

        public ReturnRequest withActualNumberOfDays(Integer actualNumberOfDays) {
            return new ReturnRequest(returnConfirmation.getInitialCharge(), returnConfirmation.getCustomer(), returnConfirmation.getMovie(), returnConfirmation.getPlannedNumberOfDays(), actualNumberOfDays);
        }


    }


}
