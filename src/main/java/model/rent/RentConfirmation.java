package model.rent;

import lombok.Value;
import model.Customer;
import model.Movie;

import java.math.BigDecimal;

@Value
public class RentConfirmation {
    private final BigDecimal initialCharge;
    private final Customer customer;
    private final Movie movie;
    private final Integer plannedNumberOfDays;


    @java.beans.ConstructorProperties({"initialCharge", "customer", "movie", "plannedNumberOfDays"})
    RentConfirmation(BigDecimal initialCharge, Customer customer, Movie movie, Integer plannedNumberOfDays) {
        this.initialCharge = initialCharge;
        this.customer = customer;
        this.movie = movie;
        this.plannedNumberOfDays = plannedNumberOfDays;

    }

    public static RentConfirmationBuilder from(RentRequest rentRequest) {
        return new RentConfirmationBuilder(rentRequest);
    }


    public static class RentConfirmationBuilder {
        private RentRequest rentRequest;


        public RentConfirmationBuilder(RentRequest rentRequest) {
            this.rentRequest = rentRequest;
        }

        public RentConfirmation withInitialCharge(BigDecimal initialCharge) {
            return new RentConfirmation(initialCharge, rentRequest.getCustomer(), rentRequest.getMovie(), rentRequest.getPlannedNumberOfDays());
        }

    }
}
