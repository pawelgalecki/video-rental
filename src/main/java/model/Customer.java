package model;

import lombok.Value;

@Value
public class Customer {
    private final String firstName;
    private final String lastName;
    private final String id;
}
