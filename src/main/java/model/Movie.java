package model;

import lombok.Value;

import java.util.UUID;
@Value
public class Movie {
    private final String title;
    private final MovieType type;
    private final UUID id;

    public Movie(String title, MovieType type, UUID id) {
        this.title = title;
        this.type = type;
        this.id = id;
    }


    public String getTitle() {
        return title;
    }

    public UUID getId() {
        return id;
    }

    public MovieType getType() {
        return type;
    }
}
