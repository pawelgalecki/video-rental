package application.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class ReturnedMovieDTO extends MovieDTO {
    private Integer actualNumberOfDays;
}
