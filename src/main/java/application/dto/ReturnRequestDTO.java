package application.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ReturnRequestDTO {
    List<ReturnedMovieDTO> returnedMovies;
    CustomerDTO customer;


    @JsonCreator
    public ReturnRequestDTO(@JsonProperty("returnedMovies") List<ReturnedMovieDTO> returnedMovies, @JsonProperty("customer") CustomerDTO customer) {
        this.returnedMovies = returnedMovies;
        this.customer = customer;
    }


}




