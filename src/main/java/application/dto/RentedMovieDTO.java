package application.dto;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class RentedMovieDTO {
    private BigDecimal initialCharge;
    private MovieDTO movie;
    private Integer plannedNumberOfDays;


}