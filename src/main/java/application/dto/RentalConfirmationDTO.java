package application.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class RentalConfirmationDTO {
    private List<RentedMovieDTO> rentedMovies;
    private BigDecimal total;
    private CustomerDTO customer;
}
