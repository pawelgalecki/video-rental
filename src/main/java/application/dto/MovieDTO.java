package application.dto;

import lombok.Data;

@Data
public class MovieDTO {
    private String title;
    private MovieTypeDTO type;
    private String id;
}
