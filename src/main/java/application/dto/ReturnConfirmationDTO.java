package application.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Data
public class ReturnConfirmationDTO {
    private  CustomerDTO customerDTO;
    private List<SingleMovieReturnConfirmationDTO> returnedMovies;
    private BigDecimal totalExtraCharge;
}
