package application.dto;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;
@Data
public class RentRequestDTO {
    CustomerDTO customer;
    private Map<String, Integer> request = new HashMap<>();


    public RentRequestDTO(String title, Integer numberOfDays) {
        request.put(title, numberOfDays);
    }
    public RentRequestDTO add(String title, Integer numberOfDays) {
        request.put(title, numberOfDays);
        return this;
    }

    private RentRequestDTO() {
    }


}
