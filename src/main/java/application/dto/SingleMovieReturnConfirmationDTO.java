package application.dto;

import lombok.Data;

import java.math.BigDecimal;
@Data
public class SingleMovieReturnConfirmationDTO {
    private BigDecimal extraCharge;
    private Integer extraDays;
    private MovieDTO movie;
}
