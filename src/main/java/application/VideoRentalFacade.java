package application;

import application.dto.*;
import model.Customer;
import model.Movie;
import model.MovieType;
import model.rent.RentConfirmation;
import model.rent.ReturnConfirmation;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import services.VideoRental;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class VideoRentalFacade {
    @Autowired
    Mapper mapper;

    private VideoRental videoRental;

    @Autowired
    public VideoRentalFacade(VideoRental videoRental) {
        this.videoRental = videoRental;
    }

    public RentalConfirmationDTO rentMovies(RentRequestDTO rentRequestDTO) {
        ArrayList<RentConfirmation> rentedMovies = new ArrayList<>();
        Customer customer = mapper.map(rentRequestDTO.getCustomer(), Customer.class);

        rentRequestDTO.getRequest().forEach((title, numberOfDays) -> {
            RentConfirmation rentConfirmation = videoRental.rentVideo(title, customer, numberOfDays);
            rentedMovies.add(rentConfirmation);
        });
        BigDecimal total = rentedMovies.stream().map(RentConfirmation::getInitialCharge).reduce(BigDecimal::add).get();
        RentalConfirmation confirmation = new RentalConfirmation(rentedMovies, total);
        return mapper.map(confirmation, RentalConfirmationDTO.class);
    }

    public ReturnConfirmationDTO returnMovies(ReturnRequestDTO returnRequestDTO) {
        Customer customer = mapper.map(returnRequestDTO.getCustomer(), Customer.class);
        ReturnConfirmationDTO returnConfirmationDTO = new ReturnConfirmationDTO();
        List<SingleMovieReturnConfirmationDTO> returnedMovies = new ArrayList<>();

        returnRequestDTO.getReturnedMovies().forEach((movieDTO) -> {
            Movie movie = new Movie(movieDTO.getTitle(), MovieType.valueOf(movieDTO.getType().name()), UUID.fromString(movieDTO.getId()));
            ReturnConfirmation returnConfirmation = videoRental.returnVideo(movie, customer, movieDTO.getActualNumberOfDays());
            SingleMovieReturnConfirmationDTO singleDto = mapper.map(returnConfirmation, SingleMovieReturnConfirmationDTO.class);
            returnedMovies.add(singleDto);
        });
        BigDecimal totalExtraCharge = returnedMovies.stream().map(SingleMovieReturnConfirmationDTO::getExtraCharge).reduce(BigDecimal::add).get();

        returnConfirmationDTO.setReturnedMovies(returnedMovies);
        returnConfirmationDTO.setCustomerDTO(returnRequestDTO.getCustomer());
        returnConfirmationDTO.setTotalExtraCharge(totalExtraCharge);

        return returnConfirmationDTO;

    }


}
