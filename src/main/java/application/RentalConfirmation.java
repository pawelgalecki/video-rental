package application;

import model.rent.RentConfirmation;

import java.math.BigDecimal;
import java.util.List;

public class RentalConfirmation {
    private final List<RentConfirmation> rentedMovies;
    private final BigDecimal total;

    public List<RentConfirmation> getRentedMovies() {
        return rentedMovies;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public RentalConfirmation(List<RentConfirmation> rentedMovies, BigDecimal total) {
        this.rentedMovies = rentedMovies;
        this.total = total;
    }

}
