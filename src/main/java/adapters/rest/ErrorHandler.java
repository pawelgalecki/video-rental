package adapters.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import services.exceptions.BusinessException;
import services.exceptions.MovieNotAvailableException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({BusinessException.class})
    @ResponseBody
    ResponseEntity<?> handleControllerException(Throwable ex) {
        HttpStatus status = HttpStatus.BAD_REQUEST;
        HashMap<String, Object> map = new HashMap<>();
        map.put("exception", ex.getClass());
        map.put("message", ex.getMessage());
        return new ResponseEntity<>(map, status);

    }

}
