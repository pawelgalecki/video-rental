package adapters.rest;


import application.VideoRentalFacade;
import application.dto.RentRequestDTO;
import application.dto.RentalConfirmationDTO;
import application.dto.ReturnConfirmationDTO;
import application.dto.ReturnRequestDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;

@RestController
@RequestMapping("/video-rental")
public class VideoRentalController {

    @Autowired
    VideoRentalFacade videoRentalFacade;


    @RequestMapping(value = "/rent", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public RentalConfirmationDTO rent(@RequestBody RentRequestDTO rentRequestDTO) {
        return videoRentalFacade.rentMovies(rentRequestDTO);
    }


    @RequestMapping(value = "/return", method = RequestMethod.POST, consumes = {MediaType.APPLICATION_JSON_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ReturnConfirmationDTO rent(@RequestBody ReturnRequestDTO returnRequestDTO) {
        return videoRentalFacade.returnMovies(returnRequestDTO);
    }

}
