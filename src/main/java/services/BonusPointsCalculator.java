package services;

import model.MovieType;
import model.rent.RentRequest;
import org.springframework.stereotype.Service;

@Service
public class BonusPointsCalculator {
    public Integer calculateBonusPoints(RentRequest rentRequest) {
        return rentRequest.getMovie().getType() == MovieType.NEW ? 2 : 1;
    }
}
