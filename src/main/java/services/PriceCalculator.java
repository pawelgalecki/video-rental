package services;

import model.MovieType;

import model.rent.RentRequest;
import model.rent.ReturnRequest;
import org.springframework.stereotype.Service;
import services.pricing.NewReleasesPricingStrategy;
import services.pricing.OldPricingStrategy;
import services.pricing.PricingStrategy;
import services.pricing.RegularPricingStrategy;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Service
public class PriceCalculator {
    public static final BigDecimal BASIC_PRICE= new BigDecimal(30);
    public static final BigDecimal PREMIUM_PRICE= new BigDecimal(40);

    private final Map<MovieType, PricingStrategy> pricingStrategies;


    public PriceCalculator() {

        pricingStrategies = new HashMap<>();
        pricingStrategies.put(MovieType.REGULAR, new RegularPricingStrategy(BASIC_PRICE));
        pricingStrategies.put(MovieType.OLD, new OldPricingStrategy(BASIC_PRICE));
        pricingStrategies.put(MovieType.NEW, new NewReleasesPricingStrategy(PREMIUM_PRICE));
    }

    public BigDecimal calculateInitialPriceFor(RentRequest rentRequest) {
        MovieType type = rentRequest.getMovie().getType();
        BigDecimal price = pricingStrategies.get(type).calculatePrice(rentRequest.getPlannedNumberOfDays());
        return price;
    }

    public BigDecimal calculateExtraPriceFor(ReturnRequest returnedRent) {
        BigDecimal result;
        if (returnedRent.getActualNumberOfDays()<=returnedRent.getPlannedNumberOfDays()) {
            result = BigDecimal.ZERO;
        } else {
        MovieType type = returnedRent.getMovie().getType();
        BigDecimal price = pricingStrategies.get(type).calculatePrice(returnedRent.getActualNumberOfDays());
            result = price.subtract(returnedRent.getInitialCharge());

        }
        return result;
    }
}
