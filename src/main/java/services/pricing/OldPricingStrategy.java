package services.pricing;

import java.math.BigDecimal;

public class OldPricingStrategy implements PricingStrategy {
    private final BigDecimal price;

    public OldPricingStrategy(BigDecimal price) {
        this.price = price;
    }

    @Override
    public BigDecimal calculatePrice(Integer numberOfDays) {

        return numberOfDays < 5 ? price : price.multiply(new BigDecimal(numberOfDays - 5)).add(price);


    }
}
