package services.pricing;

import java.math.BigDecimal;

public class RegularPricingStrategy implements PricingStrategy {
    private final BigDecimal price;

    public RegularPricingStrategy(BigDecimal price) {
        this.price = price;
    }

    @Override
    public BigDecimal calculatePrice(Integer numberOfDays) {
        return numberOfDays < 3 ? price : price.multiply(new BigDecimal(numberOfDays - 3)).add(price);
    }
}
