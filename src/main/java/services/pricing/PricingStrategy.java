package services.pricing;

import java.math.BigDecimal;

/**
 * Created by pawel on 2/20/16.
 */
public interface PricingStrategy {
    BigDecimal calculatePrice(Integer numberOfDays);
}
