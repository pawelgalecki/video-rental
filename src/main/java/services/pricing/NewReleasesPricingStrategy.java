package services.pricing;

import java.math.BigDecimal;

public class NewReleasesPricingStrategy implements PricingStrategy {
    private final BigDecimal price;

    public NewReleasesPricingStrategy(BigDecimal price) {
        this.price = price;
    }

    @Override
    public BigDecimal calculatePrice(Integer numberOfDays) {
        return price.multiply(new BigDecimal(numberOfDays));
    }
}
