package services;

import model.Customer;
import model.Movie;
import model.rent.RentConfirmation;
import model.rent.RentRequest;
import model.rent.ReturnConfirmation;
import model.rent.ReturnRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repositories.Inventory;
import services.exceptions.DataInconsistencyException;
import services.exceptions.MovieWasNotRentedByThisCustomerException;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class VideoRental {

    private Inventory inventory;

    private PriceCalculator priceCalculator;

    private BonusPointsCalculator bonusPointsCalculator;
    private Set<RentConfirmation> currentRentals = new HashSet<>();
    private Map<Customer, Integer> bonusPoints = new ConcurrentHashMap<>();

    @Autowired
    public VideoRental(Inventory inventory, PriceCalculator priceCalculator, BonusPointsCalculator bonusPointsCalculator) {
        this.inventory = inventory;
        this.priceCalculator = priceCalculator;
        this.bonusPointsCalculator = bonusPointsCalculator;
    }

    public RentConfirmation rentVideo(String title, Customer customer, int numberOfDays) {
        Movie movie = inventory.findAvailableCopyOf(title);
        RentRequest rentRequest = new RentRequest(customer, movie, numberOfDays);
        BigDecimal price = priceCalculator.calculateInitialPriceFor(rentRequest);
        updateBonusPoints(customer, rentRequest);
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(price);
        currentRentals.add(rentConfirmation);
        return rentConfirmation;
    }

    private void updateBonusPoints(Customer customer, RentRequest rentRequest) {
        Integer calculatedBonusPoints = bonusPointsCalculator.calculateBonusPoints(rentRequest);
        Integer currentBonusPoints = bonusPoints.getOrDefault(customer, 0);
        bonusPoints.put(customer, currentBonusPoints + calculatedBonusPoints);
    }

    public ReturnConfirmation returnVideo(Movie movie, Customer customer, Integer totalRentalPeriodInDays) {
        RentConfirmation rentConfirmation = checkThatReturnIsPossible(movie, customer);
        currentRentals.remove(rentConfirmation);
        inventory.giveBack(movie);
        ReturnRequest returnRequest = ReturnRequest.from(rentConfirmation).withActualNumberOfDays(totalRentalPeriodInDays);
        BigDecimal extraCharge = priceCalculator.calculateExtraPriceFor(returnRequest);
        return ReturnConfirmation.from(returnRequest).withExtraCharge(extraCharge);

    }

    private RentConfirmation checkThatReturnIsPossible(Movie movie, Customer customer) {
        inventory.checkThatMovieCanBeGivenBack(movie);
        return checkThatRentalExistsAndHasBeenMadeByTheSameCustomer(movie, customer);
    }

    private RentConfirmation checkThatRentalExistsAndHasBeenMadeByTheSameCustomer(Movie movie, Customer customer) {
        RentConfirmation rentConfirmation = currentRentals.stream().filter(rented -> rented.getMovie().equals(movie)).findFirst().orElseThrow(() -> new DataInconsistencyException("Inventory and rental tracking are not consistent"));
        if (!rentConfirmation.getCustomer().equals(customer)) {
            throw new MovieWasNotRentedByThisCustomerException(customer, rentConfirmation.getMovie());
        }
        return rentConfirmation;
    }
}
