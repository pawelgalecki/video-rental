package services.exceptions;

import model.Movie;

public class MovieWasNotRentedHereException extends BusinessException {
    private Movie movie;

    public MovieWasNotRentedHereException(Movie movie) {

        this.movie = movie;
    }

    @Override
    public String getMessage() {
        return String.format("%s was not rented here", movie);
    }


}
