package services.exceptions;

public class MovieNotAvailableException extends BusinessException {
    private String title;

    public MovieNotAvailableException(String title) {
        this.title = title;
    }

    @Override
    public String getMessage() {
        return String.format("There is not a single copy '%s' available now.", title);
    }
}
