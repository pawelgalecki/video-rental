package services.exceptions;

import model.Movie;

public class MovieAlreadyReturnedException extends BusinessException {

    private Movie movie;

    public MovieAlreadyReturnedException(Movie movie) {

        this.movie = movie;
    }

    @Override
    public String getMessage() {
        return String.format("%s is already returned", movie);
    }
}
