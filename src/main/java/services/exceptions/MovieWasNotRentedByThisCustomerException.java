package services.exceptions;

import model.Customer;
import model.Movie;

public class MovieWasNotRentedByThisCustomerException extends BusinessException {
    private Customer customer;
    private Movie movie;

    public MovieWasNotRentedByThisCustomerException(Customer customer, Movie movie) {
        this.customer = customer;
        this.movie = movie;
    }

    @Override
    public String getMessage() {
        return String.format("%s is not the one who rented %s", customer, movie);
    }
}
