package services.exceptions;

public class TitleUnknownException extends BusinessException {
    private String title;

    public TitleUnknownException(String title) {

        this.title = title;
    }

    @Override
    public String getMessage() {
        return "Title "+ title +" is not in the movies catalogue";
    }
}
