package repositories;

import model.Movie;
import model.MovieType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import services.exceptions.MovieAlreadyReturnedException;
import services.exceptions.MovieNotAvailableException;
import services.exceptions.MovieWasNotRentedHereException;
import services.exceptions.TitleUnknownException;

import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.ThrowableAssert.catchThrowable;

public class InventoryTest {
    private Inventory sut;

    @BeforeMethod
    public void setup() {
        sut = new Inventory();
    }

    @Test
    public void whenTheOnlyCopyOfAMovieIsRentedTheMovieCannotBeRentedAgain() {
        //given
        sut.addMovie("Dancing withExtraCharge wolves", MovieType.OLD, UUID.randomUUID());

        //when

        sut.findAvailableCopyOf("Dancing withExtraCharge wolves");
        Throwable thrown = catchThrowable(() -> sut.findAvailableCopyOf("Dancing withExtraCharge wolves"));

        //then
        assertThat(thrown).isInstanceOf(MovieNotAvailableException.class);
    }

    @Test
    public void whenUnrecognizedTitleIsRequestedInventoryMustTellTheRequestor() {
        //given
        sut.addMovie("Dancing withExtraCharge wolves", MovieType.OLD, UUID.randomUUID());

        //when
        Throwable thrown = catchThrowable(() -> sut.findAvailableCopyOf("Dancing withExtraCharge wolves 3"));

        //then
        assertThat(thrown).isInstanceOf(TitleUnknownException.class);

    }

    @Test
    public void whenTheOnlyCopyOfAMovieIsReturnedAfterRentalItCantBeRentedAgain() throws Exception {
        //given

        sut.addMovie("Dancing withExtraCharge wolves", MovieType.OLD, UUID.randomUUID());

        //when
        Movie movie = sut.findAvailableCopyOf("Dancing withExtraCharge wolves");
        sut.giveBack(movie);

        //then
        assertThat(sut.findAvailableCopyOf("Dancing withExtraCharge wolves")).isEqualTo(movie);

    }

    @Test
    public void sameMovieCannotBeReturnedTwice() throws Exception {
        //given
        sut.addMovie("Dancing withExtraCharge wolves", MovieType.OLD, UUID.randomUUID());

        //when
        Movie movie = sut.findAvailableCopyOf("Dancing withExtraCharge wolves");
        sut.giveBack(movie);
        Throwable thrown = catchThrowable(() -> sut.giveBack(movie));
        //then
        assertThat(thrown).isInstanceOf(MovieAlreadyReturnedException.class);

    }

    @Test
    public void movieHasToBeRentedBeforeItCanBeReturned() throws Exception {
        //given
        Movie movie = new Movie("Dancing withExtraCharge wolves", MovieType.OLD, UUID.randomUUID());

        //when
        Throwable thrown = catchThrowable(() -> sut.giveBack(movie));
        //then
        assertThat(thrown).isInstanceOf(MovieWasNotRentedHereException.class);

    }

}
