package services;

import model.Customer;
import model.Movie;
import model.MovieType;
import model.rent.RentConfirmation;
import model.rent.RentRequest;
import model.rent.ReturnConfirmation;
import model.rent.ReturnRequest;
import org.mockito.*;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import repositories.Inventory;

import java.math.BigDecimal;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;


public class VideoRentalTest {
    private VideoRental sut;
    private Customer customer = new Customer("John", "Doe", "ss:000-00-0000");
    private Movie movie = new Movie("Dancing withExtraCharge wolves", MovieType.OLD, randomUUID());
    @Mock
    private Inventory inventory;
    @Mock
    private PriceCalculator priceCalculator;

    private BonusPointsCalculator bonusPointsCalculator = new BonusPointsCalculator();


    @BeforeClass
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @BeforeMethod
    public void setup() {
        sut = new VideoRental(inventory, priceCalculator, bonusPointsCalculator);
    }

    @AfterMethod
    public void tearDown() {
        Mockito.reset(inventory);
    }

    @Captor
    ArgumentCaptor<RentRequest> rentRequestArgumentCaptor;

    @Captor
    ArgumentCaptor<ReturnRequest> returnRequestArgumentCaptor;

    @Test
    public void whenMovieIsReturnedExtraChargeIsCalculated() {
        //given
        int plannedNumberOfDays = 10;
        int actualNumberOfDays = 20;

        given(inventory.findAvailableCopyOf("Dancing withExtraCharge wolves")).willReturn(movie);
        BigDecimal initialCharge = new BigDecimal(100);
        BigDecimal extraCharge = new BigDecimal(200);
        given(priceCalculator.calculateInitialPriceFor(rentRequestArgumentCaptor.capture())).willReturn(initialCharge);
        given(priceCalculator.calculateExtraPriceFor(returnRequestArgumentCaptor.capture())).willReturn(extraCharge);
        RentConfirmation rentConfirmation = sut.rentVideo("Dancing withExtraCharge wolves", customer, 10);

        //when
        ReturnConfirmation result = sut.returnVideo(movie, customer, actualNumberOfDays);

        //then
        assertThat(result.getPlannedNumberOfDays()).isEqualTo(plannedNumberOfDays);
        assertThat(result.getActualNumberOfDays()).isEqualTo(actualNumberOfDays);
        assertThat(result.getMovie().getTitle()).isEqualTo("Dancing withExtraCharge wolves");
        assertThat(rentRequestArgumentCaptor.getValue().getPlannedNumberOfDays()).isEqualTo(plannedNumberOfDays);
        assertThat(returnRequestArgumentCaptor.getValue().getActualNumberOfDays()).isEqualTo(actualNumberOfDays);
        assertThat(result.getInitialCharge()).isEqualTo(initialCharge).isEqualTo(rentConfirmation.getInitialCharge());
        assertThat(result.getExtraCharge()).isEqualTo(extraCharge);


    }


    @Test
    public void whenNewReleaseIsRentedPriceIsCalculatedAccordingly() {
        //given
        given(inventory.findAvailableCopyOf("Dancing withExtraCharge wolves")).willReturn(movie);
        given(priceCalculator.calculateInitialPriceFor(any())).willReturn(new BigDecimal(200));


        //when
        RentConfirmation result = sut.rentVideo("Dancing withExtraCharge wolves", customer, 10);
        //then
        assertThat(result.getPlannedNumberOfDays()).isEqualTo(10);
        assertThat(result.getMovie().getTitle()).isEqualTo("Dancing withExtraCharge wolves");
        assertThat(result.getInitialCharge()).isEqualTo(new BigDecimal(200));


    }


}