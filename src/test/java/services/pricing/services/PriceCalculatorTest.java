package services.pricing.services;

import model.Customer;
import model.Movie;
import model.MovieType;
import model.rent.RentRequest;
import model.rent.RentConfirmation;
import model.rent.ReturnRequest;

import org.testng.annotations.Test;
import services.PriceCalculator;
import services.pricing.NewReleasesPricingStrategy;
import services.pricing.OldPricingStrategy;
import services.pricing.RegularPricingStrategy;

import java.math.BigDecimal;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;


public class PriceCalculatorTest {
    PriceCalculator sut = new PriceCalculator();


    @Test
    public void chargesExtraWhenMovieIsReturnedLate() {
        //given
        int plannedNumberOfDays = 1;
        int actualNumberOfDays = 2;
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "a123"), new Movie("Dancing With Wolves", MovieType.NEW, randomUUID()), plannedNumberOfDays);
        //when
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(new BigDecimal(40));
        ReturnRequest returnedRent = ReturnRequest.from(rentConfirmation).withActualNumberOfDays(actualNumberOfDays);
        BigDecimal extraCharge = sut.calculateExtraPriceFor(returnedRent);
        //then
        assertThat(extraCharge).isEqualTo(new BigDecimal(40));
    }

    @Test
    public void doesNotChargeExtraWhenMovieIsReturnedOnTime() {
        //given
        int plannedNumberOfDays = 10;
        int actualNumberOfDays = 10;
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "a123"), new Movie("Dancing With Wolves", MovieType.NEW,randomUUID()), plannedNumberOfDays);
        //when
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(new BigDecimal(100));
        ReturnRequest returnedRent = ReturnRequest.from(rentConfirmation).withActualNumberOfDays(actualNumberOfDays);
        BigDecimal extraCharge = sut.calculateExtraPriceFor(returnedRent);
        //then
        assertThat(extraCharge).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    public void estimatesPremiumPrice() {
        //given
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "a123"), new Movie("Dancing With Wolves", MovieType.NEW,randomUUID()), 10);
        //when
        BigDecimal result = sut.calculateInitialPriceFor(rentRequest);
        //then
        assertThat(result).isEqualTo(new NewReleasesPricingStrategy(PriceCalculator.PREMIUM_PRICE).calculatePrice(10));
    }

    @Test
    public void estimatesRegularPrice() {
        //given
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "a123"), new Movie("Dancing With Wolves", MovieType.REGULAR,randomUUID()), 10);
        //when
        BigDecimal result = sut.calculateInitialPriceFor(rentRequest);
        //then
        assertThat(result).isEqualTo(new RegularPricingStrategy(PriceCalculator.BASIC_PRICE).calculatePrice(10));
    }

    @Test
    public void estimatesOldPrice() {
        //given
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "a123"), new Movie("Dancing With Wolves", MovieType.OLD,randomUUID()), 10);
        //when
        BigDecimal result = sut.calculateInitialPriceFor(rentRequest);
        //then
        assertThat(result).isEqualTo(new OldPricingStrategy(PriceCalculator.BASIC_PRICE).calculatePrice(10));

    }


}