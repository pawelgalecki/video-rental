package services.pricing;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import services.pricing.OldPricingStrategy;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class OldPricingStrategyTest {
    public static final BigDecimal BASIC_PRICE = BigDecimal.valueOf(30L);
    OldPricingStrategy sut = new OldPricingStrategy(BASIC_PRICE);


    @DataProvider
    public Object[][] prices() {
        return new Object[][] {
                {1,BigDecimal.valueOf(30L)},
                {2,BigDecimal.valueOf(30L)},
                {3,BigDecimal.valueOf(30L)},
                {4,BigDecimal.valueOf(30L)},
                {5,BigDecimal.valueOf(30L)},
                {6,BigDecimal.valueOf(60L)},
                {7,BigDecimal.valueOf(90L)},
                {8,BigDecimal.valueOf(120L)},
                {9,BigDecimal.valueOf(150L)},
                {10,BigDecimal.valueOf(180L)}
        };
    }

    @Test(dataProvider = "prices")
    public void calculatesPriceWhenRenting(int numberOfDays, BigDecimal expected) {
        //when
        BigDecimal result = sut.calculatePrice(numberOfDays);
        //then
        assertThat(result).isEqualTo(expected);

    }

}