package services.pricing;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import services.pricing.RegularPricingStrategy;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class RegularPricingStrategyTest {
    public static final BigDecimal BASIC_PRICE = BigDecimal.valueOf(30L);
    RegularPricingStrategy sut = new RegularPricingStrategy(BASIC_PRICE);


    @DataProvider
    public Object[][] prices() {
        return new Object[][] {
                {1,BigDecimal.valueOf(30L)},
                {2,BigDecimal.valueOf(30L)},
                {3,BigDecimal.valueOf(30L)},
                {4,BigDecimal.valueOf(60L)},
                {5,BigDecimal.valueOf(90L)},
                {6,BigDecimal.valueOf(120L)},
                {7,BigDecimal.valueOf(150L)},
                {8,BigDecimal.valueOf(180L)},
                {9,BigDecimal.valueOf(210L)},
                {10,BigDecimal.valueOf(240L)},
                {11,BigDecimal.valueOf(270L)}
        };
    }

    @Test(dataProvider = "prices")
    public void calculatesPriceWhenRenting(int numberOfDays, BigDecimal expected) {
        //when
        BigDecimal result = sut.calculatePrice(numberOfDays);
        //then
        assertThat(result).isEqualTo(expected);

    }

}