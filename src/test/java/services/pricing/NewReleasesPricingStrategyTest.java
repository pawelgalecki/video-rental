package services.pricing;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import services.pricing.NewReleasesPricingStrategy;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;


public class NewReleasesPricingStrategyTest {
    public static final BigDecimal PREMIUM_PRICE = BigDecimal.valueOf(40L);
    NewReleasesPricingStrategy sut = new NewReleasesPricingStrategy(PREMIUM_PRICE);


    @DataProvider
    public Object[][] prices() {
        return new Object[][] {
                {1,BigDecimal.valueOf(40L)},
                {2,BigDecimal.valueOf(80L)},
                {3,BigDecimal.valueOf(120L)},
                {4,BigDecimal.valueOf(160L)},
                {5,BigDecimal.valueOf(200L)},
                {6,BigDecimal.valueOf(240L)},
                {7,BigDecimal.valueOf(280L)},
                {8,BigDecimal.valueOf(320L)},
                {9,BigDecimal.valueOf(360L)},
                {10,BigDecimal.valueOf(400L)}
        };
    }

    @Test(dataProvider = "prices")
    public void calculatesPriceWhenRenting(int numberOfDays, BigDecimal expected) {
        //when
        BigDecimal result = sut.calculatePrice(numberOfDays);
        //then
        assertThat(result).isEqualTo(expected);

    }

}