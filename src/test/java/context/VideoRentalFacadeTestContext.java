package context;

import application.VideoRentalFacade;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import services.VideoRental;

@ImportResource("classpath:dozerConfig.xml")
@Configuration
public class VideoRentalFacadeTestContext {
    @Bean
    VideoRentalFacade sut() {
        return new VideoRentalFacade(videoRental());
    }

    @Bean
    public VideoRental videoRental() {
        return  Mockito.mock(VideoRental.class);
    }
}
