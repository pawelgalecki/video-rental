package video;

import application.dto.*;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import model.MovieType;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import repositories.Inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;
import static java.util.UUID.randomUUID;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.core.IsEqual.equalTo;

@SpringApplicationConfiguration(VideoRentalApplication.class)
@WebIntegrationTest("server.port:0")
public class VideoRentalIT extends AbstractTestNGSpringContextTests {
    @Value("${local.server.port}")
    private int port;

    @Autowired
    Inventory inventory;
    private final UUID starWars3UUID = randomUUID();
    private final UUID crossingOverUUID = randomUUID();
    private final UUID godsOfEgyptUUID = randomUUID();
    private final UUID matrix11UUID = randomUUID();
    private final UUID spiderManUUID = randomUUID();
    private final UUID spiderMan2UUID = randomUUID();
    private final UUID outOfAfricaUUID = randomUUID();

    @BeforeClass
    public void setPort() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.port = port;
        RestAssured.basePath = "video-rental";
        populateVideoRentalInventory();
    }

    private void populateVideoRentalInventory() {
        inventory.addMovie("Star Wars 3", MovieType.OLD, starWars3UUID);
        inventory.addMovie("Crossing Over", MovieType.REGULAR, crossingOverUUID);
        inventory.addMovie("Gods of Egypt", MovieType.NEW, godsOfEgyptUUID);
        inventory.addMovie("Matrix 11", MovieType.NEW, matrix11UUID);
        inventory.addMovie("Spider Man", MovieType.REGULAR, spiderManUUID);
        inventory.addMovie("Spider Man 2", MovieType.REGULAR, spiderMan2UUID);
        inventory.addMovie("Out of Africa", MovieType.OLD, outOfAfricaUUID);
    }

    @Test
    public void rentsMultipleVideos() {
        //given

        CustomerDTO customerDTO = getCustomerDTO();

        RentRequestDTO rentRequestDTO = new RentRequestDTO("Matrix 11", 1);
        rentRequestDTO.add("Spider Man", 5);
        rentRequestDTO.add("Spider Man 2", 2);
        rentRequestDTO.add("Out of Africa", 7);
        rentRequestDTO.setCustomer(customerDTO);

        given()
                .contentType(ContentType.JSON).body(rentRequestDTO)
                //when
                .when().post("/rent")
                //then
                .then().assertThat()
                .body("total", equalTo(250))
                .body("rentedMovies.initialCharge.collect().sum()", equalTo(250))
                .body("rentedMovies.movie.title.collect()", hasItems("Spider Man", "Spider Man 2", "Out of Africa"))
                .body("customer.id", equalTo("9sq8aus98as"));
    }

    @Test(dependsOnMethods = "rentsMultipleVideos")
    public void returnsMultipleVideos() {
        //given
        ReturnedMovieDTO movieDTO1 = new ReturnedMovieDTO();
        movieDTO1.setId(spiderManUUID.toString());
        movieDTO1.setTitle("Spider Man");
        movieDTO1.setType(MovieTypeDTO.REGULAR);
        movieDTO1.setActualNumberOfDays(10);

        ReturnedMovieDTO movieDTO2 = new ReturnedMovieDTO();
        movieDTO2.setId(spiderMan2UUID.toString());
        movieDTO2.setTitle("Spider Man 2");
        movieDTO2.setType(MovieTypeDTO.REGULAR);
        movieDTO2.setActualNumberOfDays(12);

        List<ReturnedMovieDTO> movieDTOList = new ArrayList<>();
        movieDTOList.add(movieDTO1);
        movieDTOList.add(movieDTO2);
        CustomerDTO customerDTO = getCustomerDTO();
        ReturnRequestDTO returnRequestDTO = new ReturnRequestDTO(movieDTOList, customerDTO);

        given()
                .contentType(ContentType.JSON).body(returnRequestDTO)
                //when
                .when().post("/return")
                //then
                .then().log().all()
                .body("returnedMovies[0].extraCharge", equalTo(150))
                .body("returnedMovies[0].extraDays", equalTo(5))
                .body("returnedMovies[0].movie.id", equalTo(spiderManUUID.toString()))
                .body("returnedMovies[1].extraCharge", equalTo(270))
                .body("returnedMovies[1].extraDays", equalTo(10))
                .body("returnedMovies[1].movie.id", equalTo(spiderMan2UUID.toString()))
                .body("totalExtraCharge", equalTo(420));

    }

    @Test(dependsOnMethods = "returnsMultipleVideos")
    public void attemptToRentAVideoThatIsAlreadyRented() {
        //given
        CustomerDTO customerDTO = getCustomerDTO();
        RentRequestDTO rentRequestDTO = new RentRequestDTO("Out of Africa", 1);
        rentRequestDTO.setCustomer(customerDTO);


        given()
                .contentType(ContentType.JSON).body(rentRequestDTO)
                //when
                .when().post("/rent")
                .then()
                .body("exception", containsString("MovieNotAvailableException"))
                .body("message", Matchers.equalTo("There is not a single copy 'Out of Africa' available now."));

    }

    @Test(dependsOnMethods = "attemptToRentAVideoThatIsAlreadyRented")
    public void attemptToReturnAVideoThatIsAlreadyReturned() {


        //given
        ReturnedMovieDTO movieDTO1 = new ReturnedMovieDTO();
        movieDTO1.setId(spiderManUUID.toString());
        movieDTO1.setTitle("Spider Man");
        movieDTO1.setType(MovieTypeDTO.REGULAR);
        movieDTO1.setActualNumberOfDays(10);

        List<ReturnedMovieDTO> movieDTOList = new ArrayList<>();
        movieDTOList.add(movieDTO1);
        CustomerDTO customerDTO = getCustomerDTO();
        ReturnRequestDTO returnRequestDTO = new ReturnRequestDTO(movieDTOList, customerDTO);

        given()
                .contentType(ContentType.JSON).body(returnRequestDTO)
                //when
                .when().post("/return")
                //then
                .then()
                .body("exception", containsString("MovieAlreadyReturnedException"))
                .body("message", Matchers.equalTo(String.format("Movie(title=Spider Man, type=REGULAR, id=%s) is already returned",spiderManUUID)));


    }

    @Test(dependsOnMethods = "attemptToRentAVideoThatIsAlreadyRented")
    public void attemptToReturnAVideoByADifferentCustomer() {


        //given
        ReturnedMovieDTO movieDTO1 = new ReturnedMovieDTO();
        movieDTO1.setId(outOfAfricaUUID.toString());
        movieDTO1.setTitle("Out of Africa");
        movieDTO1.setType(MovieTypeDTO.OLD);
        movieDTO1.setActualNumberOfDays(10);

        List<ReturnedMovieDTO> movieDTOList = new ArrayList<>();
        movieDTOList.add(movieDTO1);
        CustomerDTO customerDTO = getCustomerDTO();
        customerDTO.setFirstName("Richard");
        customerDTO.setLastName("Smith");
        customerDTO.setId("aaaaaaa");
        ReturnRequestDTO returnRequestDTO = new ReturnRequestDTO(movieDTOList, customerDTO);

        given()
                .contentType(ContentType.JSON).body(returnRequestDTO)
                //when
                .when().post("/return")
                //then
                .then()
                .body("exception", containsString("MovieWasNotRentedByThisCustomer"))
                .body("message", Matchers.equalTo(String.format("Customer(firstName=Richard, lastName=Smith, id=aaaaaaa) is not the one who rented Movie(title=Out of Africa, type=OLD, id=%s)",outOfAfricaUUID)));


    }

    @Test(dependsOnMethods = "attemptToRentAVideoThatIsAlreadyRented")
    public void attemptToReturnAVideoThatIsNotRecognized() {


        //given
        ReturnedMovieDTO movieDTO1 = new ReturnedMovieDTO();
        movieDTO1.setId("042ed11b-3a0f-4408-87e9-066c78622240");
        movieDTO1.setTitle("Out of Africa 2");
        movieDTO1.setType(MovieTypeDTO.NEW);
        movieDTO1.setActualNumberOfDays(10);

        List<ReturnedMovieDTO> movieDTOList = new ArrayList<>();
        movieDTOList.add(movieDTO1);
        CustomerDTO customerDTO = getCustomerDTO();
        ReturnRequestDTO returnRequestDTO = new ReturnRequestDTO(movieDTOList, customerDTO);

        given()
                .contentType(ContentType.JSON).body(returnRequestDTO)
                //when
                .when().post("/return")
                //then
                .then()
                .body("exception", containsString("MovieWasNotRentedHereException"))
                .body("message", equalTo("Movie(title=Out of Africa 2, type=NEW, id=042ed11b-3a0f-4408-87e9-066c78622240) was not rented here"));


    }




    private CustomerDTO getCustomerDTO() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("John");
        customerDTO.setLastName("Doe");
        customerDTO.setId("9sq8aus98as");
        return customerDTO;
    }

}
