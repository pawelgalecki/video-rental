package application.dto;


import application.RentalConfirmation;
import model.Customer;
import model.Movie;
import model.MovieType;
import model.rent.RentRequest;
import model.rent.RentConfirmation;
import model.rent.ReturnConfirmation;
import model.rent.ReturnRequest;
import org.assertj.core.api.iterable.Extractor;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collections;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;

@ContextConfiguration("classpath:dozerConfig.xml")
public class DozerMappingTest extends AbstractTestNGSpringContextTests {

    @Autowired
    Mapper mapper;

    @Test
    public void dozerMapsFromModelToDto() {
        //given
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "21ss12"), new Movie("Crossing Over", MovieType.OLD,randomUUID()), 10);
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(BigDecimal.TEN);
        //when
        RentedMovieDTO rentedMovieDTO = mapper.map(rentConfirmation, RentedMovieDTO.class);
        //then
        assertThat(rentedMovieDTO.getMovie().getTitle()).isEqualTo("Crossing Over");
        assertThat(rentedMovieDTO.getMovie().getType()).isEqualTo(MovieTypeDTO.OLD);
    }

    @Test
    public void dozerMapsFromRentalConfirmationToRentalConfirmationDTO() {
        //given
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "21ss12"), new Movie("Crossing Over", MovieType.OLD,randomUUID()), 10);
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(BigDecimal.TEN);


        RentalConfirmation confirmation = new RentalConfirmation(Collections.singletonList(rentConfirmation), BigDecimal.TEN);

        //when
        RentalConfirmationDTO dto = mapper.map(confirmation, RentalConfirmationDTO.class);
        //then
        assertThat(dto.getTotal()).isEqualTo("10");
        assertThat(dto.getCustomer().getFirstName()).isEqualTo("John");
        assertThat(dto.getCustomer().getLastName()).isEqualTo("Smith");
        assertThat(dto.getRentedMovies()).extracting((Extractor<RentedMovieDTO, Object>) input -> input.getMovie().getTitle()).contains("Crossing Over");
    }

    @Test
    public void dozerMapsFromCustomerDTOtoCustomer() {
        //given
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("a");
        customerDTO.setLastName("b");
        customerDTO.setId("c");
        //when
        Customer customer = mapper.map(customerDTO, Customer.class);
        //then
        assertThat(customer.getFirstName()).isEqualTo("a");
    }

    @Test
    public void mapsFromReturnConfirmationToSingleMovieReturnConfirmationDTO() {
        //given
        RentRequest rentRequest = new RentRequest(new Customer("John", "Smith", "21ss12"), new Movie("Crossing Over", MovieType.OLD,randomUUID()), 7);
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(BigDecimal.TEN);
        ReturnRequest returnRequest = ReturnRequest.from(rentConfirmation).withActualNumberOfDays(20);
        ReturnConfirmation returnConfirmation = ReturnConfirmation.from(returnRequest).withExtraCharge(new BigDecimal(70));

        //when

        SingleMovieReturnConfirmationDTO dto = mapper.map(returnConfirmation, SingleMovieReturnConfirmationDTO.class);

        //then
        assertThat(dto.getExtraCharge()).isEqualTo("70");
        assertThat(dto.getExtraDays()).isEqualTo(13);
        assertThat(dto.getMovie().getTitle()).isEqualTo("Crossing Over");
        assertThat(dto.getMovie().getType()).isEqualTo(MovieTypeDTO.OLD);
    }

}