package application;

import application.dto.*;
import context.VideoRentalFacadeTestContext;
import model.Customer;
import model.Movie;
import model.MovieType;
import model.rent.RentRequest;
import model.rent.RentConfirmation;
import model.rent.ReturnConfirmation;
import model.rent.ReturnRequest;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import services.VideoRental;

import java.math.BigDecimal;
import java.util.*;

import static java.util.UUID.randomUUID;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;

@ContextConfiguration(classes = {VideoRentalFacadeTestContext.class})
public class VideoRentalFacadeTest extends AbstractTestNGSpringContextTests {
        VideoRentalFacade sut;
        VideoRental videoRental;


    @BeforeMethod
    public void setUp() {
        videoRental = applicationContext.getBean(VideoRental.class);
        sut = applicationContext.getBean(VideoRentalFacade.class);

    }

    @Test
    public void oneCanReturnMovies() throws Exception {

        //given
        ReturnedMovieDTO movieDTO1 = new ReturnedMovieDTO();
        movieDTO1.setId(UUID.randomUUID().toString());
        movieDTO1.setTitle("Margin Call");
        movieDTO1.setType(MovieTypeDTO.OLD);
        movieDTO1.setActualNumberOfDays(10);

        ReturnedMovieDTO movieDTO2 = new ReturnedMovieDTO();
        movieDTO2.setId(UUID.randomUUID().toString());
        movieDTO2.setTitle("Matrix 11");
        movieDTO2.setType(MovieTypeDTO.NEW);
        movieDTO1.setActualNumberOfDays(12);

        List<ReturnedMovieDTO> movieDTOList= new ArrayList<>();
        movieDTOList.add(movieDTO1);
        movieDTOList.add(movieDTO2);
        CustomerDTO customerDTO = getCustomerDTO();
        ReturnRequestDTO returnRequestDTO = new ReturnRequestDTO(movieDTOList, customerDTO);

        given(videoRental.returnVideo(movieTitled("Margin Call"), any(), any())).willReturn(returnConfirmation("Margin Call", 10, 10, new BigDecimal(0)));
        given(videoRental.returnVideo(movieTitled("Matrix 11"), any(), any())).willReturn(returnConfirmation("Matrix 11", 12, 10, new BigDecimal(70)));

        //when
        ReturnConfirmationDTO returnConfirmationDTO = sut.returnMovies(returnRequestDTO);
        //then
        assertThat(returnConfirmationDTO.getCustomerDTO()).isEqualToComparingFieldByField(customerDTO);
        assertThat(returnConfirmationDTO.getReturnedMovies()).extracting(input -> input.getMovie().getTitle()).contains("Margin Call", "Matrix 11");
        assertThat(returnConfirmationDTO.getReturnedMovies()).extracting(SingleMovieReturnConfirmationDTO::getExtraDays).contains(0, 2);
        assertThat(returnConfirmationDTO.getTotalExtraCharge()).isEqualTo("70");
    }

    private CustomerDTO getCustomerDTO() {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setFirstName("Richards");
        customerDTO.setLastName("Burns");
        customerDTO.setId("9032892jdd");
        return customerDTO;
    }

    private ReturnConfirmation returnConfirmation(String movieTitle, Integer actualNumberOfDays, Integer plannedNumberOfDays, BigDecimal extraCharge) {
        RentRequest rentRequest = new RentRequest(new Customer("Richard", "Burns", "9032892jdd"), new Movie(movieTitle, MovieType.OLD,randomUUID()), plannedNumberOfDays);
        RentConfirmation rentConfirmation = RentConfirmation.from(rentRequest).withInitialCharge(BigDecimal.TEN);
        ReturnRequest returnRequest = ReturnRequest.from(rentConfirmation).withActualNumberOfDays(actualNumberOfDays);
        return ReturnConfirmation.from(returnRequest).withExtraCharge(extraCharge);
    }

    private Movie movieTitled(String title) {
        return argThat(new BaseMatcher<Movie>() {
            @Override
            public void describeTo(Description description) {

            }

            @Override
            public boolean matches(Object item) {
                return item!=null && ((Movie) item).getTitle().equals(title);
            }
        });
    }

}